class WorkoutsController < ApplicationController

  before_action :find_workout, only: [:show, :edit, :update, :destroy]

  def index
    @workouts = Workout.all
  end

  def new
    @workout=Workout.new
  end

  def show
  end

  def create
    @workout=Workout.create(workout_params)
    if @workout.save
      redirect_to @workout
    else
      render 'new'
    end
  end

  def edit
  end

  def update
    if @workout.update(workout_params)
      redirect_to @workout
    else
      render 'edit'
    end
  end

  def destroy
    @workout.destroy
    redirect_to root_path
  end

  private

    def find_workout
      @workout=Workout.find(params[:id])
    end

    def workout_params
      params.require(:workout).permit(:date, :mood, :workout, :length)
    end

end

