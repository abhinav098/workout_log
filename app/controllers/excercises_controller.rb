class ExcercisesController < ApplicationController

  before_action :find_excercise, only:[:show, :edit, :update, :destroy]

  def new
  end

  def create
  end

  def update
  end

  def destroy
  end

  private

  def find_excercise
  end

  def set_exc_params
  end

end
