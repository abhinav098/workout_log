class Workout < ActiveRecord::Base
  validates :date, presence: true
  validates :workout, presence: true
  validates :mood, presence: true
  validates :length, presence: true
  has_many :excercises
end
